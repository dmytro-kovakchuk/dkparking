﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.


using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{

    public static class Settings
    {
        public static int startBalance = 0;
        public const int numberOfPlaces = 10;
        public const int paymentPeriod = 5;
        public const int writeToLogTime = 60;
        public const double fineCoefficient = 2.5;
        public static readonly Regex RegexCheck = new Regex(@"[A-Z]{2}[-]{1}[0-9]{4}[-]{1}[A-Z]{2}");

        public static decimal Tariff(VehicleType vehicle)
        {
            switch (vehicle)
            {
                case VehicleType.PassengerCar:
                    return 2;
                case VehicleType.Bus:
                    return (decimal)3.5;
                case VehicleType.Truck:
                    return 5;
                case VehicleType.Motorcycle:
                    return 1;
            }

            return 0;
        }


    }
}