﻿using System;
using CoolParking.BL.Models;
using System.Text.RegularExpressions;
using Newtonsoft.Json;


// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

namespace CoolParking.BL.Models
{
    public class Vehicle
    {   
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; private set; }

        [JsonProperty("balance")]
        public decimal Balance { get; internal set; }



        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (Settings.RegexCheck.IsMatch(id))
            {
                Id = id;
            }
            else
            {
                throw new ArgumentException("Enter valid Id like 'AA-0004'");
            }

            if (balance >= 0)
            {
                Balance = balance;
            }
            else
            {
                throw new ArgumentException();
            }

            if ((int)vehicleType >= 0 && (int)vehicleType <= 3)
            {
                VehicleType = vehicleType;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        



        public static string GenerateRandomRegistrationPlateNumber()
        {
            string NumericPartOfId = GenerateRandom.GenerateRandomNumber();
            string RandomId = GenerateRandom.GenerateRandomChars()  + "-" + NumericPartOfId + '-' + GenerateRandom.GenerateRandomChars() ;
            return RandomId;
        }
    }

    public static class GenerateRandom
    {

        public static string GenerateRandomChars()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var rnd = new Random();
            var result = chars[rnd.Next(26)].ToString() + chars[rnd.Next(26)].ToString();
            return result;

        }
        public static string GenerateRandomNumber()
        {
            var rnd = new Random();
            string num = rnd.Next(10).ToString() + rnd.Next(10).ToString() + rnd.Next(10).ToString() + rnd.Next(10).ToString();
           
            return num;
        }
        
    }
}
