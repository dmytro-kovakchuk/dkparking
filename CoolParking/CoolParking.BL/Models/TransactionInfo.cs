﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.


using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {

        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }
        [JsonProperty("Sum")]
        public decimal Sum { get; set; }
        [JsonIgnore]
        public decimal AmountOfMoney { get; set; }
       


        public TransactionInfo(string vehicleId, decimal amountOfMoney)
        {
            VehicleId = vehicleId;
            AmountOfMoney = amountOfMoney;
            Sum += AmountOfMoney;
        }



    }
}
