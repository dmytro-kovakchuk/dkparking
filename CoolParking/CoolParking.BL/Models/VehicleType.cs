﻿// TODO: implement enum VehicleType.
//       Items: PassengerCar, Truck, Bus, Motorcycle.

using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace CoolParking.BL.Models
{
    public enum VehicleType
    {
        
        PassengerCar,
        Truck,
        Bus,
        Motorcycle,
    }
}

