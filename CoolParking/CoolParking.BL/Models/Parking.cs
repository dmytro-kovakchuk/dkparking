﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.


using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking Instance;
        public decimal Balance { get; set; }
        public List<Vehicle> VehiclesList { get; set; }

        private Parking( List<Vehicle> vehiclesList)
        {
            Balance = Settings.startBalance;
            VehiclesList = vehiclesList;
        }
        public static Parking GetInstance( List<Vehicle> vehiclesList)
        {
            return Instance ?? (Instance = new Parking(vehiclesList));
        }


    }

}