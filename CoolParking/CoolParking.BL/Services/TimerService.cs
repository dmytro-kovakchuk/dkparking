﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System;
using System.Threading;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Threading.Tasks;


namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public event ElapsedEventHandler Elapsed;
        public double Interval { get; set; }

        private System.Timers.Timer _timer;

        public TimerService(int interval)
        {
            Interval = interval;
            _timer = new System.Timers.Timer(Interval * 1000);
            _timer.AutoReset = true;
            _timer.Elapsed += FireElapsedEvent;
            _timer.Start();
        }

        public void FireElapsedEvent(object sender, ElapsedEventArgs e)
        {
            Elapsed?.Invoke(this, null);
        }
        public void Start()
        {
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}