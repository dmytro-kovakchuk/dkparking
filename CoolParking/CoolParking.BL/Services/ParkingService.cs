﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.


using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Linq;
using System.Threading.Channels;
using System.Timers;
using System.Collections;

namespace CoolParking.BL.Services
{
    public class ParkingService: IParkingService
    {
        private Parking Parking;
        private ILogService logService;
        private ITimerService _withdrawTimer;
        private ITimerService _logTimer;

        public List<TransactionInfo> TransactionList;
        public ParkingService(ITimerService _withdrawTimer, ITimerService _logTimer, ILogService _logService)
        {
            Parking = Parking.GetInstance(new List<Vehicle>());
            logService = _logService;
            this._logTimer = _logTimer;
            this._withdrawTimer = _withdrawTimer;
            TransactionList = new List<TransactionInfo>();
            
            _withdrawTimer.Elapsed += delegate(object sender, ElapsedEventArgs e)
            {
                decimal amountOfMoney;

                for (int i = 0; i < Parking.VehiclesList.Count; i++)
                {
                    
                    var standartTariff = Settings.Tariff(Parking.VehiclesList[i].VehicleType); ;
                    if (Parking.VehiclesList[i].Balance >= standartTariff)
                    {
                        amountOfMoney = standartTariff;
                        Parking.Balance += amountOfMoney;
                        Parking.VehiclesList[i].Balance -= amountOfMoney;
                    }
                    else if (Parking.VehiclesList[i].Balance < standartTariff && Parking.VehiclesList[i].Balance > 0)
                    {
                        amountOfMoney = Parking.VehiclesList[i].Balance + (standartTariff - Parking.VehiclesList[i].Balance) * (decimal)Settings.fineCoefficient;
                        Parking.Balance += amountOfMoney;
                        Parking.VehiclesList[i].Balance -= amountOfMoney;
                    }
                    else
                    {
                        amountOfMoney = standartTariff * (decimal)Settings.fineCoefficient;
                        Parking.Balance += amountOfMoney;
                        Parking.VehiclesList[i].Balance -= amountOfMoney;
                    }


                    if (TransactionList.Count == 0)
                    {
                        TransactionList.Add(new TransactionInfo(Parking.VehiclesList[i].Id, amountOfMoney));
                    }
                    else
                    {
                        TransactionList.Add(new TransactionInfo(Parking.VehiclesList[i].Id, amountOfMoney));
                    }
                }

            };



            _logTimer.Elapsed += delegate (object sender, ElapsedEventArgs e)
            {
                var transactionList = new List<TransactionInfo>();
                transactionList.AddRange(TransactionList);
                TransactionList.Clear();
                
                foreach (var item in transactionList)
                {
                    var LogString = item.VehicleId + " " + item.AmountOfMoney;
                    _logService.Write(LogString);
                }
                if(transactionList.Count==0)
                {
                    _logService.Write("Transaction list is clear");
                }
            };
        }

       

        public void Dispose()
        {
            Parking.Balance = 0;
            Parking.VehiclesList.Clear();
            TransactionList.Clear();
            _logTimer?.Dispose();
            _withdrawTimer?.Dispose();
        }

        public decimal GetBalance()
        {
            return Parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.numberOfPlaces;
        }

        public int GetFreePlaces()
        {
            return Settings.numberOfPlaces - Parking.VehiclesList.Count;
        }

        public List<Vehicle> GetVehicles()
        {
            return Parking.VehiclesList;
        }
        /*
        Виправиви також ось це, просто також забув про цю перевірку. Неуважний трошки(((

         -> CoolParking.BL.Tests.ParkingServiceTests: AddVehicle_WhenNewVehicleAndVehiclesCountIsMax_ThenThrowInvalidOperationException
        Assert.Throws() Failure
        Expected: typeof(System.InvalidOperationException)
        Actual:   (No exception was thrown)
         */

        public void AddVehicle(Vehicle vehicle)
        {
            if(Parking.VehiclesList.Count == Settings.numberOfPlaces)
            {
                throw new InvalidOperationException();
            }
            if (Parking.VehiclesList.FirstOrDefault(x=>x.Id==vehicle.Id) != null)
            {
                throw new ArgumentException();
            }
            Parking.VehiclesList.Add(vehicle);
        }
        /*
         В останній перевірці не пройшов наступний тест, адже зовсім забув про перевірку на негативний баланс, але одразу її додав, тому тепер тест має пройти
        
        -> CoolParking.BL.Tests.ParkingServiceTests: RemoveVehicle_WhenExistingVehicleWithNegativeBalance_ThenThrowInvalidOperationException

         */
        public void RemoveVehicle(string vehicleId)
        {
            var deletedItem = Parking.VehiclesList.FirstOrDefault(x => x.Id == vehicleId);
            if (deletedItem == null)
            {
                throw new ArgumentException();
            }
            else
            {
                if (deletedItem.Balance >= 0)
                {
                    Parking.VehiclesList.Remove(deletedItem);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {

            var SelectedVehicle = this.Parking.VehiclesList.FirstOrDefault(x => x.Id == vehicleId);
            if (SelectedVehicle == null)
            {
                throw new ArgumentException();
            }
            if (sum > 0)
            {
                SelectedVehicle.Balance = SelectedVehicle.Balance + sum;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        /* Також отримав ось таку помилку
         -> CoolParking.BL.Tests.ParkingServiceTests: GetLastParkingTransactions_When3VehiclesAfter2WithdrawTimeouts_Then6Transactions
        Assert.Equal() Failure
        Expected: 6
        Actual:   1

        Бо не зрозумів як саме має працювати дана функція, тому передававсаме останню транзакцію із списку.А не весь список. І через Sum також працювала по іншому в  ній я записував якраз
        суму всіх попередніх з нинішньою, а не просто нинішню. Ну тобто воно проходило зарпоновані тести, але не пройшло вказаний вище, але тепер все б мало працювати я переробив. Але знову 
        ж такм не хотів відволікати тому написав тут 
         */


        public TransactionInfo[] GetLastParkingTransactions()
        {
            var transactionList = new  TransactionInfo[TransactionList.Count];
            TransactionList.CopyTo(transactionList);
            return transactionList;
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }
        public Vehicle GetVehicle(string id)
        {
            var SelectedVehicle = Parking.VehiclesList.FirstOrDefault(x => x.Id == id);
            return SelectedVehicle;
        }
    }
}