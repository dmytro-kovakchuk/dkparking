﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System.Web;
using System.Text.RegularExpressions;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : Controller
    {
        private ParkingService parkingService;
        public VehiclesController(ParkingService parking)
        {
            parkingService = parking;
        }

        [HttpGet]
        public List<Vehicle> GetVehicles()
        {
            return parkingService.GetVehicles();
        }


        [HttpGet("{id}")]
        public Vehicle GetAction(string id)
        {
            if (Settings.RegexCheck.IsMatch(id))
            {
                var listOfVehice = parkingService.GetVehicles();
                var item = listOfVehice.FirstOrDefault(x => x.Id == id);
                if (item != null)
                {
                    return item;
                }
                else
                {
                    Response.StatusCode = 404;
                    return null;
                }
            }
            else
            {
                Response.StatusCode = 400;
                return null;
            }

        }
        /*
         * Не знаю чи точно правильно так як зробив я, адже провірку зазвичай роблять на стороні клієнта і це
         * був найкращий варіант, з тих, що я зміг знайти
         */

        [HttpPost]
        public Vehicle AddVehice(VehicleCheck FakeVehicleForNormalCheck)
        {
            try
            {
                var item = new Vehicle(FakeVehicleForNormalCheck.Id, FakeVehicleForNormalCheck.VehicleType, FakeVehicleForNormalCheck.Balance);
                parkingService.AddVehicle(item);
                Response.StatusCode = 201;
                return item;
            }
            catch
            {
                Response.StatusCode = 400;
                return null;
            }

        }

        [HttpDelete("{id}")]

        public void DeleteVehicle(string id)
        {
            if (Settings.RegexCheck.IsMatch(id))
            {
                var listOfVehice = parkingService.GetVehicles();
                var item = listOfVehice.FirstOrDefault(x => x.Id == id);
                if (item != null)
                {
                    parkingService.RemoveVehicle(id);
                    Response.StatusCode = 204;
                }
                else
                {
                    Response.StatusCode = 404;
                }
            }
            else
            {
                Response.StatusCode = 400;

            }
        }

    }
}
