﻿using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : Controller
    {
        private ParkingService parkingService;
        public ParkingController(ParkingService parking)
        {
            parkingService = parking;
        }

        [Route("balance"), HttpGet]
        public decimal CheckBalance()
        {
            return parkingService.GetBalance();
        }

        [Route("capacity")]
        [HttpGet]
        public int GetAction()
        {
            return parkingService.GetCapacity();
        }

        [Route("freePlaces")]
        [HttpGet]
        public int GetFreePlaces()
        {
            return parkingService.GetFreePlaces();
        }
    }
}
