﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.WebAPI.Models;
using System.Text.Json;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class TransactionsController : Controller
    {
        private ParkingService parkingService;
        public TransactionsController(ParkingService parking)
        {
            parkingService = parking;
        }
        [Route("last")]
        [HttpGet]
        public TransactionInfo[] GetLastTransaction()
        {
            return parkingService.GetLastParkingTransactions();
        }


        [Route("all")]
        [HttpGet]
        public string GetAction()
        {
            try
            {
                return parkingService.ReadFromLog();
            }
            catch
            {
                Response.StatusCode = 404;
                return null;
            }
        }

        [Route("topUpVehicle")]
        [HttpPut]
        /*
          На цьому методі провалив тести, бо забув заьрати з конструктору провірку параметрів, бо
        в якийсь момент відкатився до старого коміту і воно видавало Exeption при передачі параметру

        Але вже коли я отримав останній результат автотесту, я це одразу виправив і не став турбувати
        викладачів. Але помилку я зрозумів.

        Єдине що довелося створити оремий клас для того, адже я не знаю як прийняти одразу декілька параметрів з тіла запиту
        адже [FromBody] можна застосувати лище до одного параметру, інакше отримаємо помилку.Я спробував через стрінг передати. А потім зі стрінгJSON конвертувавти
        , але такоє не вийшло, тому зробив так. Напевно це в якісь мірі заперечує
        принципу DRY, тому якщо це не зовсім правильно хотів би у відповіді до роботи дізнатися як треба було

        Але взагалі такі провірки зазвичай на фронті роблять

         */
        public Vehicle Put(TopUp item)
        {
            if (!item.TopUpCheckParams())
            {
                Response.StatusCode = 400;
                return null;
            }

            try
            {
                parkingService.TopUpVehicle(item.Id, item.Sum);
                return parkingService.GetVehicle(item.Id);
            }
            catch
            {

                Response.StatusCode = 404;
                return null;
            }
        }

    }
}
