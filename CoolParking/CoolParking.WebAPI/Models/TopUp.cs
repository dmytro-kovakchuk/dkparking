﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Models
{
    public class TopUp
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        public TopUp(string id, decimal sum)
        {
            Id = id;
            Sum = sum;
        }

        public bool TopUpCheckParams()
        {
            if (!Settings.RegexCheck.IsMatch(Id))
            {
                return false;
            }

            if (Sum < 0)
            {
                return false;
            }

            return true;


        }
    }
}
