﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Models
{
    /* Створив повністю новий клас, адже не зміг винести перевірку з класу Vehicle,адже воно б не пройшло переевірки до ".NET ecosystem"
     А унаслідувати не зміг, бо все одно ж викликаю базовий конструктор
     */

    public class VehicleCheck 
    {
        [JsonProperty("id")]
        public string Id { get; private set; }
        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; private set; }

        [JsonProperty("balance")]
        public decimal Balance { get; internal set; }

        public VehicleCheck(string id, VehicleType vehicleType, decimal balance)
        {
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

    }
}
