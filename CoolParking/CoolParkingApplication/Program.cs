﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParkingApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            string _logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
            var _withdrawTimer = new TimerService(5);
            var _logTimer = new TimerService(60);
            var _logService = new LogService(_logFilePath);
            var _parkingService = new ParkingService(_withdrawTimer, _logTimer, _logService);

            var vehicle1 = new Vehicle("DD-0001-DD", VehicleType.Truck, 0);
            var vehicle2 = new Vehicle("AA-0002-AA", VehicleType.Bus, 0);
           _parkingService.AddVehicle(vehicle1);
            _parkingService.AddVehicle(vehicle2);
            var exit = false;

            do {

                PrintAllOperations();
                var GetOperation = Console.ReadLine();
                
                switch (GetOperation)
                {
                    case "1":
                        {
                            Console.Clear();
                            Console.WriteLine("Parking Balance: " + _parkingService.GetBalance());
                            Console.ReadKey();
                        }
                        break;
                    case "2":
                        {
                            Console.Clear();
                            Console.WriteLine("Sum of Transactions for this period: " + _parkingService.GetLastParkingTransactions()[0].Sum);
                            Console.ReadKey();
                        }
                        break;
                    case "3":
                        {
                            Console.WriteLine("Number of free places:" + _parkingService.GetFreePlaces());
                            Console.ReadKey();
                        }
                        break;
                    case "4":
                        {
                            Console.WriteLine("All transaction from last period");
                            var transactionList = _parkingService.TransactionList;
                            foreach (var transaction in transactionList)

                            {
                                Console.WriteLine("Vehicle Id:  " + transaction.VehicleId + " Amount Of Money:  " + transaction.Sum);
                            }
                            Console.ReadKey();
                        }
                        break;
                    case "5":
                        {
                            Console.WriteLine("History of all transaction:");
                            var history = _parkingService.ReadFromLog();
                            Console.WriteLine(history);
                            Console.ReadKey();
                        }
                        break;
                    case "6":
                        {
                            Console.WriteLine("List of Vehicle:");
                            var vehicleList = _parkingService.GetVehicles();
                            foreach (var item in vehicleList)
                            {
                                Console.WriteLine("Id: " + item.Id + " Type: " + item.VehicleType + " Balance: " + item.Balance);
                            }
                            Console.ReadKey();
                        }
                        break;
                    case "7":
                        {
                            var vehicleId = InputVehicleIdWithValidation();
                            var vehicleType = TakeVehicleType();
                            var balance = InputValidBalance();
                            _parkingService.AddVehicle(new Vehicle(vehicleId, vehicleType, balance));
                            Console.ReadKey();
                        }
                        break;
                    case "8":
                        {
                            var vehicleId = InputVehicleIdWithValidation();
                            try
                            {
                                _parkingService.RemoveVehicle(vehicleId);
                            }
                            catch (ArgumentException)
                            {
                                Console.Clear();
                                Console.WriteLine("This car isn't on parking");
                            }
                            
                            Console.ReadKey();
                        }
                        break;
                    case "9":
                        {
                            var vehicleId = InputVehicleIdWithValidation();

                            var topUpBalance = InputValidBalance(); 

                            try
                            {
                                _parkingService.TopUpVehicle(vehicleId, topUpBalance);
                                Console.WriteLine("На рахунку автомобыля тепер " + _parkingService.GetVehicles().FirstOrDefault(x => x.Id == vehicleId).Balance);
                                Console.ReadKey();
                            }
                            catch (ArgumentException)
                            {
                                Console.Clear();
                                Console.WriteLine("This car isn't on parking");
                            }
                            Console.ReadKey();
                        }
                        break;
                    case "0":
                        {
                            exit = true;
                        }break;
                    default:
                        {
                            PrintAllOperations();
                        }
                        break;
                }

            } while (exit!=true);

        }

        public static void PrintAllOperations ()
        {
            Console.Clear();
            Console.WriteLine("1 - Print Parking Balance");
            Console.WriteLine("2 - Print Sum of Transactions for this period");
            Console.WriteLine("3 - Print number of Free Places");
            Console.WriteLine("4 - Print List Of Transactions of last period");
            Console.WriteLine("5 - Print All Transactions history ");
            Console.WriteLine("6 - Print Vehicle List");
            Console.WriteLine("7 - Add Vehicle to the Parking");
            Console.WriteLine("8 - Remove Vehicle from Parking");
            Console.WriteLine("9 - Top Up Vehicle Balance");
            Console.WriteLine("0 - Exit");

          
        }

        public static VehicleType TakeVehicleType()
        {
            Console.WriteLine("Chooice a type of Vehicle");
            Console.WriteLine("1 - PassengerCar");
            Console.WriteLine("2 - Truck");
            Console.WriteLine("3 - Bus");
            Console.WriteLine("4 - Motorcycle \n");

            var chooice = Console.ReadLine();
            switch (chooice)
            {
                case "1":
                    return VehicleType.PassengerCar;
                case "2":
                    return VehicleType.Truck;
                case "3":
                    return VehicleType.Bus;
                case "4":
                    return VehicleType.Motorcycle;
                default:
                    {
                        var mistake = TakeVehicleType();
                        return mistake;
                    }
            }
        }

        public static string InputVehicleIdWithValidation()
        {
            string vehicleId;

            Console.WriteLine("Enter VehicleId like AA-0003-AA");
            vehicleId = Console.ReadLine();
            var idValidator = new Regex(@"[A-Z]{2}[-]{1}[0-9]{4}[-]{1}[A-Z]{2}");

            if(idValidator.IsMatch(vehicleId))
            {
                return vehicleId;
            }

            Console.Clear();
            Console.WriteLine("You inptu invalid Id Try Again");
            vehicleId = InputVehicleIdWithValidation();
            
            return vehicleId;
        }

        public static decimal InputValidBalance()
        {
            decimal balance;
            Console.WriteLine(" Enter the amount of money");
            try
            {
                 balance = Convert.ToDecimal(Console.ReadLine());
                if (balance >= 0)
                {
                    return balance;
                }
                Console.Clear();
                Console.WriteLine("You can't add negative value");
                Console.WriteLine("Try Again");
                balance = InputValidBalance();

                return balance;
            }
            catch(FormatException)
            {
                Console.Clear();
                Console.WriteLine("Value must be а number");
                
            }

            balance = InputValidBalance();
            return balance;
        }


    }
}
